/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import data.Ihs;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author d
 */
public class IhsCsvFileReader {

    public List<Ihs> readCSV(String uri) {
        List<Ihs> siteList = null;

        try {
            CSVReader reader = new CSVReader(new FileReader(uri));
            // List pages = reader.readAll();

            CsvToBean<Ihs> siteBean = new CsvToBean<Ihs>();

            Map<String, String> columnMapping = new HashMap<>();
            
            columnMapping.put("Area", "Area");
            columnMapping.put("Service Unit", "ServiceUnit");
            columnMapping.put("ASUFAC", "ASUFAC");
            columnMapping.put("Facility Name", "FacilityName");
            columnMapping.put("Facility Category", "FacilityCategory");
            columnMapping.put("Facility Type", "FacilityType");
            columnMapping.put("Behavioral Health", "BehavioralHealth");
            columnMapping.put("Dental", "Dental");
            columnMapping.put("I T U Facility", "ITUFacility");
            columnMapping.put("APC Flag", "APCFlag");
            columnMapping.put("Status", "Status");
            columnMapping.put("Hours Of Operation", "HoursOfOperation");
            columnMapping.put("Street", "Street");
            columnMapping.put("City", "City");
            columnMapping.put("State", "State");
            columnMapping.put("Zip", "Zip");
            columnMapping.put("General Telephone", "GeneralTelephone");
            columnMapping.put("General Fax", "GeneralFax");
            columnMapping.put("Website URL", "WebsiteURL");
            columnMapping.put("Lat Geographic WGS84", "LatGeographicWGS84");
            columnMapping.put("Lon Geographic WGS84", "LonGeographicWGS84");

            HeaderColumnNameTranslateMappingStrategy<Ihs> strategy = new HeaderColumnNameTranslateMappingStrategy<Ihs>();
            strategy.setType(Ihs.class);
            strategy.setColumnMapping(columnMapping);

//        CSVReader reader = new CSVReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(uri)));
            siteList = siteBean.parse(strategy, reader);

//            ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();
//            strat.setType(NewPage.class);
//            String[] columns = new String[]{"url"}; // the fields to bind do in your JavaBean
//            strat.setColumnMapping(columns);
//
//            CsvToBean csv = new CsvToBean();
//            siteList = csv.parse(strat, reader);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return siteList;

    }
}
