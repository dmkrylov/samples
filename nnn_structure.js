// This javascript is also included on all pages of the system via our theme.info file
// Changing the location of this file will need to be reflected in that file as well
(function($) {
  Drupal.applicationIntegration = {};

//   Drupal.applicationIntegration.showAll = function(){
//     $('div.pulse-div > iframe').show();
//   };



  Drupal.behaviors.applicationIntegration = {
    attach: function(context) {

      /*
       * Toggles show all hide all iframes for the PULSE integration section
       * TODO: make this generic for other applications
       * 
       */
      $(".action-toggle-all", context).click(function() {
        // Toggle the wording of the link
        if ($("#pulse-view-all").text() === "View All") {
          $("#pulse-view-all").text("Hide All");
          // Toggle the display of the iframes to be showing
          $('.pulse-div > iframe').show();
          // Set the a tag expand/collapse object to be close option
          $('.pulse-div > a').text("-");
        } else {
          $("#pulse-view-all").text("View All");
          // Toggle the display of the iframes to be hidden
          $('.pulse-div > iframe').hide();
          // Set child a tag text to be expand option
          $('.pulse-div > a').text("+");
        }

      });

      /*
       * Toggle only the selected iframe
       * ISSUE: special case when all are manual opened or closed the View All button doesn't change and would 
       * require a double click to Hide All if all are manual opened
       */
      $(".action-toggle", context).click(function() {
        // Toggle the wording of the link
        if ($(this).text() === "+") {
          $(this).text("-");
        } else {
          $(this).text("+");
        }
        // Toggle the display of the iframe that is a sibling to this object
        $(this).siblings("iframe").toggle();
      });
    }
  };
})(jQuery);

