/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapping;

import dao.DataLoad;
import data.BmissProdAddress;
import data.BmissProdSite;
import data.Ihs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author d
 */
public class MapGeo {
    
    DataLoad dl = new DataLoad();
    HashMap<String, List<String>> bmissIhsGeoMap = new HashMap();
    
        public HashMap<String, List<String>> matchGeoBmissProdVsPphc() {
        
        int BPHCMatchedCount = 0;
        int IHSMatchedCount = 0;
        Boolean BPHCmatchFlag;
        Boolean IHSMatchFlag;
        
        for (BmissProdSite bmissSite : dl.BmissSiteProdList) {
            BPHCmatchFlag = false;
            IHSMatchFlag = false;
            for (BmissProdAddress bmissAddress : dl.BmissAddressProdList) {
                if (bmissSite.getSiteID()
                        .equals(bmissAddress.getSiteID())) {
                    
                    for (Ihs ihs : dl.IhsList) {
                        if (Matching.matchGeo(bmissAddress.getLatitude(), bmissAddress.getLongitude(),
                                ihs.getLatGeographicWGS84(), ihs.getLonGeographicWGS84(), 5)) {
                            if (!bmissSite.getSiteID().isEmpty() && !ihs.getASUFAC().isEmpty()
                              && bmissSite.getSiteID() != null    && ihs.getASUFAC() != null) {

                                List<String> innerList = bmissIhsGeoMap.containsKey(
                                        bmissSite.getSiteID()) ? 
                     bmissIhsGeoMap.get(bmissSite.getSiteID()) : new ArrayList<String>();
                                innerList.add(ihs.getASUFAC());
                                bmissIhsGeoMap.put(bmissSite.getSiteID(), innerList);

                            }
                            IHSMatchFlag = true;
//                            System.out.println(bmissSite.getSiteID()
//                                    + " " + bmissSite.getSiteName()
//                                    + " " + bmissAddress.getGeocodedAddressInput().substring(0, 50)
//                                    + " <-BMISSprod-IHS-> "
//                                    + ihs.getASUFAC() 
//                                    + " " + ihs.getFacilityName()
//                                    + " " + ihs.getStreet() + " " + ihs.getState()
//                                    + " : " + bmissAddress.getLatitude()
//                                    + " " + bmissAddress.getLongitude()
//                                    + "==" + ihs.getLatGeographicWGS84()
//                                    + " " + ihs.getLonGeographicWGS84());
                        }
                    }
                }
            }
            if (BPHCmatchFlag) BPHCMatchedCount++;
            if (IHSMatchFlag) IHSMatchedCount++;
        }
        System.out.println("GEO MATCHING: BPHC matched: " + BPHCMatchedCount + "/" + dl.BphcList.size()
                + " IHS mathed: " + IHSMatchedCount + "/" + dl.IhsList.size()
                + " -- out of Bmiss prod " + dl.BmissSiteProdList.size());
        
        return bmissIhsGeoMap;
    }


    
}
