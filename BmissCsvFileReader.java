/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import data.Bmiss;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author d
 */
public class BmissCsvFileReader  {
    
        public List<Bmiss> readCSV(String uri) {
        List<Bmiss> siteList = null;

        try {
            CSVReader reader = new CSVReader(new FileReader(uri));
       // List pages = reader.readAll();

            CsvToBean<Bmiss> siteBean = new CsvToBean<Bmiss>();

            Map<String, String> columnMapping = new HashMap<>();
            columnMapping.put("SiteID", "SiteID");
            columnMapping.put("SiteName", "SiteName");
            columnMapping.put("Address", "Address");
            columnMapping.put("City", "City");
            columnMapping.put("StateAbbrev", "StateAbbrev");
            columnMapping.put("ZIP", "ZIP");
            columnMapping.put("Status", "Status");

            HeaderColumnNameTranslateMappingStrategy<Bmiss> strategy = new HeaderColumnNameTranslateMappingStrategy<Bmiss>();
            strategy.setType(Bmiss.class);
            strategy.setColumnMapping(columnMapping);

//        CSVReader reader = new CSVReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(uri)));
            siteList = siteBean.parse(strategy, reader);

//            ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();
//            strat.setType(NewPage.class);
//            String[] columns = new String[]{"url"}; // the fields to bind do in your JavaBean
//            strat.setColumnMapping(columns);
//
//            CsvToBean csv = new CsvToBean();
//            siteList = csv.parse(strat, reader);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return siteList;

    }
}
