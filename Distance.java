
package geo;

import org.geotools.referencing.GeodeticCalculator;
import java.awt.geom.Point2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author d
 */
public class Distance {

    public double getDistance(String LatA, String LonA, 
                                    String LatB, String LonB) {

        GeodeticCalculator calc = new GeodeticCalculator();

        Point2D a = new Point2D.Double(Double.parseDouble(LonA), Double.parseDouble(LatA));
        Point2D b = new Point2D.Double(Double.parseDouble(LonB), Double.parseDouble(LatB));
        calc.setStartingGeographicPoint(a);
        calc.setDestinationGeographicPoint(b);

     //   System.out.println("Distance : " + calc.getOrthodromicDistance() / 1000 + " kms");
        
        try {
        return calc.getOrthodromicDistance() / 1000;
        }
        catch(Exception ex) {
             System.out.println("Distance calculation exception " + ex);
            return 100000000;
        }

    }

}
