/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import data.Bmiss;
import data.BmissProdAddress;
import data.BmissProdPhone;
import data.BmissProdSite;
import data.Bphc;
import data.Ihs;
import java.util.List;

/**
 *
 * @author d
 */
public class DataLoad {

    private final BphcCsvFileReader pfr = new BphcCsvFileReader();
    private final BmissCsvFileReader mfr = new BmissCsvFileReader();
    private final IhsCsvFileReader ifr = new IhsCsvFileReader();
    private final BmissProdSiteCsvReader bpfr = new BmissProdSiteCsvReader();
    private final BmissProdAddressCsvReader bafr = new BmissProdAddressCsvReader();
    private final BmissProdPhoneCsvReader bphfr = new BmissProdPhoneCsvReader();

    public final List<Bphc> BphcList;
    public final List<Bmiss> BmissList;
    public final List<Ihs> IhsList;
    public final List<BmissProdSite> BmissSiteProdList;
    public final List<BmissProdAddress> BmissAddressProdList;
    public final List<BmissProdPhone> BmissPhoneProdList;

    public DataLoad() {
        BphcList = pfr.readCSV("/home/d/PROJECTS/NNN/BPHC.csv");
        System.out.println("size of file " + BphcList.size()
                + " first row " + BphcList.get(0).toString());

        BmissList = mfr.readCSV("/home/d/PROJECTS/NNN/BMISS.csv");
        System.out.println("size of file " + BmissList.size()
                + " first row " + BmissList.get(0).toString());

        IhsList = ifr.readCSV("/home/d/PROJECTS/NNN/IHS.csv");
        System.out.println("size of file " + IhsList.size()
                + " first row " + IhsList.get(0).toString());

        BmissSiteProdList = bpfr.readCSV("/home/d/PROJECTS/NNN/bmiss-site-data/site.csv");
        System.out.println("size of file " + BmissSiteProdList.size()
                + " first row " + BmissSiteProdList.get(8563).toString());

        BmissAddressProdList = bafr.readCSV("/home/d/PROJECTS/NNN/bmiss-site-data/address.csv");
        System.out.println("size of file " + BmissAddressProdList.size()
                + " first row " + BmissAddressProdList.get(8563).toString());

        BmissPhoneProdList = bphfr.readCSV("/home/d/PROJECTS/NNN/bmiss-site-data/phone.csv");
        System.out.println("size of file " + BmissPhoneProdList.size()
                + " first row " + BmissPhoneProdList.get(0).toString());
    }

}
